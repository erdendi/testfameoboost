import Homefullslider from './homefullslider'

describe('@components/homefullslider', () => {
  it('exports a valid component', () => {
    expect(Homefullslider).toBeAComponent()
  })
})
