export default function({ app, redirect }) {
  // If the user is not authenticated
  // if (!store.state.auth) {
  if (!app.$cookiz.get('auth')) {
    return redirect('/login')
  }
}
