import { notification } from 'ant-design-vue'

export const __notification = (type, title, message) => {
  if (type === 'success') {
    notification.success({
      message: title || 'Sukses',
      description: message || 'Data berhasil di simpan!',
    })
  } else if (type === 'error') {
    notification.error({
      message: title || 'Error',
      description: message || 'Error',
    })
  } else if (type === 'warn') {
    notification.warn({
      message: title,
      description: message,
    })
  } else {
    notification.warning({
      message: title,
      description: message,
    })
  }
}
