import i18n from './i18n'

export default {
  server: {
    //port: 3000, // default: 3000
    port: 8080, // default: 3000
    host: '0.0.0.0', // default: localhost,
    timing: false,
  },
  googleAnalytics: {
    id: 'UA-184017563-1',
    autoTracking: {
      screenview: true,
    },
  },
  publicRuntimeConfig: {
    googleAnalytics: {
      id: 'UA-184017563-1',
    },
  },
  /*
   ** Nuxt rendering mode
   ** See https://nuxtjs.org/api/configuration-mode
   */
  ssr: false,
  target: 'static',
  /*
   ** Headers of the page
   */
  head: {
    title: 'Fameoboost | Smart-AI Influencer Marketing Platform',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      {
        hid: 'description',
        name: 'description',
        content:
          'Fameoboost, Influencer marketing campaigns For Every Budget! Get Loud! Get Creative! Get More Conversions!',
      },
      {
        hid: 'keywords',
        name: 'keywords',
        content:
          'fameo, fameoboost, influencer, kol, influencer marketing, endorsement',
      },
      { property: 'og:url', content: 'https://fameoboost.com/' },
      {
        property: 'og:site_name',
        content: 'Fameoboost | Smart-AI Influencer Marketing Platform',
      },
      {
        property: 'og:title',
        content: 'Fameoboost | Smart-AI Influencer Marketing Platform',
      },
      { name: 'twitter:card', content: 'summary_large_image' },
      {
        name: 'twitter:description',
        content:
          'Fameoboost, Influencer marketing campaigns For Every Budget! Get Loud! Get Creative! Get More Conversions!',
      },
      {
        name: 'twitter:title',
        content: 'Fameoboost | Smart-AI Influencer Marketing Platform',
      },
      { name: 'twitter:image', content: 'MASUKIN LINK KE LOGO' },
      {
        name: 'google-site-verification',
        content: 'oiL1ms7WvX8rsyNi-HUoQp2-4PKoEYfM6mnoEGiVi6M',
      },
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      {
        rel: 'stylesheet',
        href:
          'https://fonts.googleapis.com/css2?family=Poppins:wght@300;400;600;700&display=swap',
      },
      {
        rel: 'canonical',
        href: 'https://fameoboost.com/',
      },
    ],
  },
  /*
   ** Customize the progress-bar color
   */
  loading: { color: '#57D4FF' },
  /*
   ** Active link class for the router
   */
  router: {
    linkExactActiveClass: 'exact-active-link',
  },
  /*
   ** Generate dynamic pages
   */
  generate: {
    fallback: true, // fallback to spa mode - default is 200.html
    exclude: [/.unit/], // exclude unit tests from being generated
  },
  /*
   ** Global CSS
   */
  css: ['ant-design-vue/dist/antd.css', '~assets/scss/global.scss'],
  /*
   ** Plugins to load before mounting the App
   */
  plugins: [
    { src: '~plugins/vue-carousel', ssr: false },
    '~/plugins/lazy-load-images.js',
    '~/plugins/filters.js',
    '~/plugins/currency.js',
    '~/plugins/vuelidate.js',
    '@/plugins/antd-ui',
    '@/plugins/axios',
    { src: '~plugins/ga.js', mode: 'client' },
  ],
  /*
   ** Nuxt.js modules
   */
  robots: () => {
    return {
      UserAgent: '*',
      Disallow: '/',
    }
  },
  modules: [
    // Doc: https://axios.nuxtjs.org/usage
    // Simple usage
    '@nuxtjs/robots',
    // With options
    [
      '@nuxtjs/robots',
      {
        /* module options */
      },
    ],
    '@nuxtjs/axios',
    // '@nuxtjs/pwa',
    ['@nuxtjs/pwa', { icon: false }],
    '@nuxtjs/eslint-module',
    ['nuxt-i18n', i18n],
    '@nuxt/content',
    '@nuxtjs/style-resources',
    // '@nuxtjs/proxy',
    ['cookie-universal-nuxt', { alias: 'cookiz' }],
    '@nuxtjs/recaptcha',
    '@nuxtjs/google-analytics',
    '@nuxtjs/robots',
    'nuxt-facebook-pixel-module',
  ],
  components: true,
  recaptcha: {
    hideBadge: false, // Hide badge element (v3 & v2 via size=invisible)
    siteKey: '6LeKK-8ZAAAAACvh4JQ9jGUSETyAcvfdEmMahoH6', // Site key for requests
    version: 2, // Version
    size: 'normal', // Size: 'compact', 'normal', 'invisible' (v2)
  },

  // globally load all our sass variables
  // styleResources: {
  //   scss: ['./assets/scss/variables.scss'],
  // },
  // purgeCSS: {
  //   mode: 'postcss',
  //   whitelistPatterns: [/cookie-consent/],
  // },
  /*
   ** Axios module configuration
   ** See https://axios.nuxtjs.org/options
   */
  axios: {
    // https: true,
    // proxy: false,
    // proxy: 'https://fameoboost-lngnht3bqq-as.a.run.app',
  },
  auth: {
    strategies: {
      local: {
        endpoints: {
          login: { url: '/login', method: 'post', propertyName: 'data.token' },
          user: { url: '/user/me', method: 'get', propertyName: 'data' },
          logout: false,
        },
      },
    },
  },
  /*
   ** Build configuration
   */
  // build: {
  //   postcss: {
  //     plugins: {
  //       tailwindcss: './tailwind.config.js',
  //     },
  //   },
  //   /*
  //    ** You can extend webpack config here
  //    */
  //   extend(config, ctx) {},
  // },
  styleResources: {
    scss: ['./assets/scss/*.scss'],
  },
  script: [
    // { src: 'pixel.js', type: 'text/javascript' },
    { src: './linkedin.js', type: 'text/javascript' },
  ],
  facebook: {
    /* module options */
    pixelId: '166638544593320',
    autoPageView: true,
  },
}
