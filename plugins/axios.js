export default function({ store, $axios, app }, inject) {
  // Create a custom axios instance
  const api = $axios.create({
    headers: {
      common: {
        // Accept: 'application/json, */*',
        // 'x-api-key':
        //   'eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE1OTY3MjIyMTQsInN1YiI6ImF1dGgiLCJpYXQiOjE1OTY2MzU4MTQsImF1ZCI6InRlc3QiLCJpc3MiOiJkY2FmIn0.G0y624Ydwhb5TvIHEUdxiwH-q0rbBJf5txNv4AXX0fw',
        // 'Access-Control-Allow-Origin': '*',
      },
    },
  })

  // Set baseURL to something different
  api.setBaseURL('https://fameoboost-lngnht3bqq-as.a.run.app')

  if (app.$cookiz.get('auth')) {
    const { accessToken } = app.$cookiz.get('auth')
    api.setToken(accessToken, 'Bearer')
  }

  // Inject to context as $api
  inject('api', api)
}
