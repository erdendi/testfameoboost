import Simplecarousel from './simplecarousel'

describe('@components/simplecarousel', () => {
  it('exports a valid component', () => {
    expect(Simplecarousel).toBeAComponent()
  })
})
