export const formatPrice = (type, value = 0) => {
  if (type === 'round') {
    const val = (Math.round(value) / 1).toFixed(0).replace('.', ',')
    return val.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.')
  }
  if (type === 'ceil') {
    const val = (Math.ceil(value) / 1).toFixed(0).replace('.', ',')
    return val.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.')
  }
  if (type === 'rp') {
    const val = (Math.floor(value) / 1).toFixed(0).replace('.', ',')
    return 'Rp. ' + val.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.')
  }
  const val = (Math.floor(value) / 1).toFixed(0).replace('.', ',')
  return val.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.')
}

export default { formatPrice }
