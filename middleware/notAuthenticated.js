export default function({ app, redirect }) {
  // If the user is authenticated redirect to home page
  // if (store.state.auth) {
  //   return redirect('/')
  // }
  if (app.$cookiz.get('auth')) {
    return redirect('/profil-saya')
  }
}
