import Loading from './loading'

describe('@components/loading', () => {
  it('exports a valid component', () => {
    expect(Loading).toBeAComponent()
  })
})
