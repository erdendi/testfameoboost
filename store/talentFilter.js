export const state = () => ({
  category: [],
  sex: [],
  age: [],
  location: [],
  followers: [],
  price: [],
  followerLocation: [],
  followerSex: [],
  followerCategory: [],
  followerAge: [],
  maritalStatus: [],
  statusAnak: [],
})

export const mutations = {
  add(state, text) {
    state.list.push({
      text,
      done: false,
    })
  },
  remove(state, { todo }) {
    state.list.splice(state.list.indexOf(todo), 1)
  },
  toggle(todo) {
    todo.done = !todo.done
  },
}
