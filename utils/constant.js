export const ROLE = {
  ADMIN: 1,
  TALENT: 2,
  CLIENT: 3,
}

export default { ROLE }
